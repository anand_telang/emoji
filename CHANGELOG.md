# Releases / Changes

## 1.0.5

* Adds support for dynamic Emoji.assethos as a proc (ala Rails.assethos)

## 1.0.4

* Moved Homepage/repo to wpeterson/emoji

## 1.0.3

* Allow empty Emoji.assethos and more assethos formatting support

## 1.0.2

* Improved Emoji.assethos configuration/handling
* Emoji::Index cleanup and find_by_unicode
* Plaintext alt tags (optional)

## 1.0.1

* Important html_safe security fix.

## 1.0.0

* Initial gem release