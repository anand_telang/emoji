# Emoji

## Installation

First, add this line to the application's Gemfile:

``` GEMFILE
gem 'emoji'
```

Next execute:

    $ bundle

Or install it individually as:

    $ gem install emoji

Finally, install the emoji image library assets:
    
    $ rake emoji:install_assets