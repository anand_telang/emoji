# encoding: UTF-8

require 'emoji/string_ext'

describe String, 'with Emoji extensions' do
  describe '#with_emojiimg' do
    it 'should replace unicode moji with an img tag' do
      basestr = "I ❤ Emoji"
      repalcestr = basestr.with_emojiimg
      assert_equal "I <img alt=\"❤\" class=\"emoji\" src=\"http://localhost:3000/heart.png\"> Emoji", repalcestr
    end
  end

  describe '#imageurl' do
    it 'should generate imageurl' do
      assert_equal 'http://localhost:3000/cyclone.png', '🌀'.imageurl
      assert_equal 'http://localhost:3000/cyclone.png', 'cyclone'.imageurl
    end
  end

  describe '#emojidat' do
    it 'should find data for a name or a moji' do
      data_from_moji = '❤'.emojidat
      data_from_string = 'heart'.emojidat

      assert_equal data_from_moji, data_from_string
    end
  end
end
