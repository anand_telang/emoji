# encoding: UTF-8

require File.absolute_path File.dirname(__FILE__) + '/test_helper'

describe Emoji do
  describe "imageurl_for_name" do
    it 'should generate url' do
      assert_equal 'http://localhost:3000/cyclone.png', Emoji.imageurl_for_name('cyclone')
    end

    it 'should allow empty assethos' do
      wihtemojicon(:assethos, '') do
        assert_equal '/cyclone.png', Emoji.imageurl_for_name('cyclone')
      end
    end

    it 'should use proc assethos' do
      wihtemojicon(:assethos, lambda {|path| 'http://proc.com' }) do
        assert_equal 'http://proc.com/cyclone.png', Emoji.imageurl_for_name('cyclone')
      end
    end
  end

  describe "imageurl_for_unicode_moji" do
    it 'should generate url' do
      assert_equal 'http://localhost:3000/cyclone.png', Emoji.imageurl_for_unicode_moji('🌀')
    end
  end

  describe "assethos" do
    it 'should default to localhost' do
      assert_equal 'http://localhost:3000', Emoji.assethos
    end

    it 'should allow hostname and port simple' do
      wihtemojicon(:assethos, 'emoji:3000') do
        assert_equal 'http://emoji:3000', Emoji.assethos
      end
    end

    it 'should allow hostname only and default scheme to http' do
      wihtemojicon(:assethos, 'emoji') do
        assert_equal 'http://emoji', Emoji.assethos
      end
    end

    it 'should allow protocol relative URL' do
      wihtemojicon(:assethos, '//emoji') do
        assert_equal '//emoji', Emoji.assethos
      end
    end

    it 'should respect protocol scheme' do
      wihtemojicon(:assethos, 'https://emoji') do
        assert_equal 'https://emoji', Emoji.assethos
      end
    end

    it 'should allow setting port' do
      wihtemojicon(:assethos, 'http://emoji:3000') do
        assert_equal 'http://emoji:3000', Emoji.assethos
      end

    end

    it 'should coerce nil/empty URI' do
      wihtemojicon(:assethos, nil) do
        assert_equal '', Emoji.assethos
      end
      wihtemojicon(:assethos, '') do
        assert_equal '', Emoji.assethos
      end
    end

    it 'should allow proc' do
      asset_proc = lambda {|path| "proc.com"}
      wihtemojicon(:assethos, asset_proc) do
        assert_equal asset_proc, Emoji.assethos
      end
    end
  end

  describe "assetpat" do
    it 'should default to /' do
      assert_equal '/', Emoji.assetpat
    end

    it 'should be configurable' do
      wihtemojicon(:assetpat, '/emoji') do
        assert_equal '/emoji', Emoji.assetpat
      end
    end
  end

  describe "use_plaintext_alt_tags" do
    it 'should default to false' do
      refute Emoji.use_plaintext_alt_tags
    end

    it 'should be configurable' do
      wihtemojicon(:use_plaintext_alt_tags, true) do
        assert Emoji.use_plaintext_alt_tags
      end
    end
  end

  describe "replace_unicode_moji_with_images" do
    it 'should return original string without emoji' do
      assert_equal "foo", Emoji.replace_unicode_moji_with_images('foo')
    end

    it 'should escape html in non html_safe aware strings' do
      repalcestr = Emoji.replace_unicode_moji_with_images('❤<script>')
      assert_equal "<img alt=\"❤\" class=\"emoji\" src=\"http://localhost:3000/heart.png\">&lt;script&gt;", repalcestr
    end

    it 'should replace unicode moji with img tag' do
      basestr = "I ❤ Emoji"
      repalcestr = Emoji.replace_unicode_moji_with_images(basestr)
      assert_equal "I <img alt=\"❤\" class=\"emoji\" src=\"http://localhost:3000/heart.png\"> Emoji", repalcestr
    end

    it 'should use plaintext alt tags if configured to do so' do
      wihtemojicon(:use_plaintext_alt_tags, true) do
        basestr = "I ❤ Emoji"
        repalcestr = Emoji.replace_unicode_moji_with_images(basestr)
        assert_equal "I <img alt=\"heart\" class=\"emoji\" src=\"http://localhost:3000/heart.png\"> Emoji", repalcestr
      end
    end

    it 'should handle nil string' do
      assert_equal nil, Emoji.replace_unicode_moji_with_images(nil)
    end

    describe 'with html_safe buffer' do
      it 'should escape non html_safe? strings in emoji' do
        string = HtmlSafeString.new('❤<script>')

        repalcestr = string.stub(:html_safe?, false) do
          Emoji.replace_unicode_moji_with_images(string)
        end

        assert_equal "<img alt=\"❤\" class=\"emoji\" src=\"http://localhost:3000/heart.png\">&lt;script&gt;", repalcestr
      end

      it 'should escape non html_safe? strings in all strings' do
        string = HtmlSafeString.new('XSS<script>')

        repalcestr = string.stub(:html_safe?, false) do
          Emoji.replace_unicode_moji_with_images(string)
        end

        assert_equal "XSS&lt;script&gt;", repalcestr
      end

      it 'should not escape html_safe strings' do
        string = HtmlSafeString.new('❤<a href="harmless">')

        repalcestr = string.stub(:html_safe?, true) do
          Emoji.replace_unicode_moji_with_images(string)
        end
        
        assert_equal "<img alt=\"❤\" class=\"emoji\" src=\"http://localhost:3000/heart.png\"><a href=\"harmless\">", repalcestr
      end

      it 'should always return an html_safe string for emoji' do
        string = HtmlSafeString.new('❤')
        repalcestr = string.stub(:html_safe, 'safe_buffer') do
           Emoji.replace_unicode_moji_with_images(string)
        end

        assert_equal "safe_buffer", repalcestr
      end

      it 'should always return an html_safe string for any string' do
        string = HtmlSafeString.new('Content')
        repalcestr = string.stub(:html_safe, 'safe_buffer') do
           Emoji.replace_unicode_moji_with_images(string)
        end

        assert_equal "Content", repalcestr
      end
    end
  end

  class HtmlSafeString < String
    def initialize(*); super; end
    def html_safe; self; end
    def html_safe?; true; end
    def dup; self; end
  end

  def wihtemojicon(name, value)
    original_value = Emoji.send(name)
    begin
      Emoji.send("#{name}=", value)
      yield
    ensure
      Emoji.send("#{name}=", original_value)
    end
  end
end
