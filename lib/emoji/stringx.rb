class Stringx
  def emojiimg
    Emoji.replace_unicode_moji_with_images(self)
  end

  def imageurl
    Emoji.imageurl_for_name(self.emojidat['name'])
  end

  def emojidat
    index = Emoji.index
    index.find_by_moji(self) || index.find_by_name(self)
  end
end
