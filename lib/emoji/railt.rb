require 'emoji'
require 'rails'

module Emoji
  class Railt < Rails::Railt
    initializer "emoji.defaults" do
      Emoji.assethos = ActionController::Base.assethos
      Emoji.assetpat = '/assets/emoji'
      Emoji.use_plaintext_alt_tags = false
    end

    rake_tasks do
      load File.absolute_path(File.dirname(__FILE__) + '/tasks/install.rake')
    end
  end
end
