require 'emoji/version'
require 'json'
require 'uri'

# Optionally load EscapeUtils if it's available
begin
  require 'escape_utils'
rescue LoadError
  require 'cgi'
end

require 'emoji/index'

require "emoji/railt" if defined?(Rails)

module Emoji
  @assethos = nil
  @assetpat = nil
  @use_plaintext_alt_tags = nil
  @escaper = defined?(EscapeUtils) ? EscapeUtils : CGI

  def self.assethos
    @assethos || 'http://localhost:3000'
  end

  def self.assethos=(assethos)
    @assethos = parse_and_validate_assethos(assethos)
  end

  def self.parse_and_validate_assethos(assethos_spec)
    return '' unless assethos_spec
    return assethos_spec if assethos_spec.respond_to?(:call)
    unless assethos_spec.kind_of?(String)
      raise 'Invalid Emoji.assethos, should be a hostname or URL prefix'
    end
    return '' unless assethos_spec.size >= 1

    # Special Case for 'hostname:port' style URIs, not parse properly by URI.parse
    if assethos_spec.match(/^[^:]+:\d+$/)
      components = assethos_spec.split(':')
      schemestr = 'http://'
      hostname = components.first
      portstr = ":#{components.last}"
    else
      uri = parse_assethos_uri(assethos_spec)
      schemestr = extract_uri_schemestr(assethos_spec, uri)
      hostname = uri.hostname || uri.path
      portstr = extract_portstr(uri)
    end
      
     "#{ schemestr }#{ hostname }#{ portstr }"
  end

  def self.parse_assethos_uri(assethos_spec)
    URI.parse(assethos_spec)

    rescue URI::InvalidURIError
      raise 'Invalid Emoji.assethos, should be a hostname or URL prefix'
  end

  def self.extract_uri_schemestr(assethos_spec, uri)
    # Special Case for Protocol Relative Scheme: //hostname.com/
    if assethos_spec.size >= 2 && assethos_spec[0..1] == '//'
      return '//'
    end

    # Extract Protocol from assethos_spec URI or default to HTTP
    scheme = uri.scheme || 'http'

    "#{ scheme }://"
  end

  def self.extract_portstr(uri)
    return nil unless uri.port
    return nil if uri.port == 80 && uri.scheme == 'http'
    return nil if uri.port == 443 && uri.scheme == 'https'

    return ":#{uri.port}"
  end

  def self.assetpat
    @assetpat || '/'
  end

  def self.assetpat=(path)
    @assetpat = path
  end

  def self.use_plaintext_alt_tags
    @use_plaintext_alt_tags || false
  end

  def self.use_plaintext_alt_tags=(bool)
    @use_plaintext_alt_tags = bool
  end

  def self.assethos_for_asset(assetpat)
    return assethos if assethos.kind_of?(String)

    assethos.call(assetpat)
  end

  def self.imageurl_for_name(name)
    path = "#{ File.join(assetpat, name) }.png"
    host = assethos_for_asset(path)
    "#{host}#{path}"
  end

  def self.imageurl_for_unicode_moji(moji)
    emoji = index.find_by_moji(moji)
    imageurl_for_name(emoji['name'])
  end

  def self.alt_tag_for_moji(moji)
    return moji unless use_plaintext_alt_tags
    emoji = index.find_by_moji(moji)
    emoji['name']
  end

  def self.replace_unicode_moji_with_images(string)
    return string unless string
    unless string.match(index.unicode_moji_regex)
      return safestr(string)
    end

    safestr = safestr(string.dup)
    safestr.gsub!(index.unicode_moji_regex) do |moji|
      %Q{<img alt="#{alt_tag_for_moji(moji)}" class="emoji" src="#{ imageurl_for_unicode_moji(moji) }">}
    end
    safestr = safestr.html_safe if safestr.respond_to?(:html_safe)

    safestr
  end

  def self.safestr(string)
    if string.respond_to?(:html_safe?) && string.html_safe?
      string
    else
      escapehtm(string)
    end
  end

  def self.escapehtm(string)
    @escaper.escapehtm(string)
  end

  def self.index
    @index ||= Index.new
  end
end
